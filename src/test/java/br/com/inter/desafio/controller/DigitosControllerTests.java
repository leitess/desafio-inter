package br.com.inter.desafio.controller;

import br.com.inter.desafio.controllers.digitos.DigitosController;
import br.com.inter.desafio.dto.DigitoDTO;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.mapper.DigitoMapper;
import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.services.DigitoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Collections;

import static br.com.inter.desafio.utils.JsonConvertionUtils.asJsonString;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class DigitosControllerTests {

    private static final String URL_POST = "/api/digito";
    private static final String URL_GET = "/api/digitos";

    private MockMvc mockMvc;

    private DigitoMapper mapper = DigitoMapper.INSTANCE;

    @Mock
    private DigitoService digitoService;

    @InjectMocks
    private DigitosController digitosController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(digitosController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setViewResolvers((viewName, locale) -> new MappingJackson2JsonView())
                .build();
    }

    @Test
    void dadoUmNumeroEAVezesParaConcatenar_chamarAFuncaoDigitoUnico_RetornarOkStatusEOCalculo() throws Exception {
        // given
        Digito digito = Digito.builder().build();
        DigitoDTO dto = mapper.toDTO(digito);

        // when
        when(digitoService.digitoUnico("9875", 4)).thenReturn(digito);

        // then
        mockMvc.perform(post(URL_POST).param("numero", "9875").param("vezesConcatenada", String.valueOf(4))
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numero", is(dto.getNumero())))
                .andExpect(jsonPath("$.digitoUnico", is(dto.getDigitoUnico())));
    }

    @Test
    void dadoUmNumeroMenorQueUm_QuandoCamadoADigitoUnico_RetornarStatusBadRequestELancarAExcecao() throws Exception {
        // given
        Digito digito = Digito.builder().build();
        DigitoDTO dto = mapper.toDTO(digito);

        // when
        when(digitoService.digitoUnico("-10", 4)).thenThrow(ValorMinException.class);

        // then
        mockMvc.perform(post(URL_POST).param("numero", "-10").param("vezesConcatenada", String.valueOf(4))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dto))).andExpect(status().isBadRequest());
    }

    @Test
    void dadoUmNumeroMaiorQueOMaximoDoInteiro_QuandoCamadoADigitoUnico_RetornarStatusBadRequestELancarAExcecao() throws Exception {
        // given
        Digito digito = Digito.builder().build();
        DigitoDTO dto = mapper.toDTO(digito);

        // when
        when(digitoService.digitoUnico("10", 2147483647)).thenThrow(ValorMaxException.class);

        // then
        mockMvc.perform(post(URL_POST).param("numero", "10").param("vezesConcatenada", String.valueOf(2147483647))
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto))).andExpect(status().isBadRequest());
    }

    @Test
    void quandoOUsuarioQuiserLimparOHistoricoDeCalculos_RetornarOkStatus() throws Exception {

        // when
        doNothing().when(digitoService).deleteAll();

        //then
        mockMvc.perform(delete(URL_GET + "/limpar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(digitoService, times(1)).deleteAll();
    }
}
