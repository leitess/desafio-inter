package br.com.inter.desafio.utils;

import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.models.Endereco;
import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class EnderecoUtilsTests {

    @Mock
    private EnderecoUtils enderecoUtils;

    @BeforeEach
    void init(){
        enderecoUtils = new EnderecoUtils();
    }

    @Test
    public void quandoForDadoUmObjetoString_ChamarAFuncaoToJson_RetornUmObjetoJson() throws FormatoDeCepErradoException, CepNaoEncontrado {
        // given
        Gson g = new Gson();
        String endereco = enderecoUtils.getCep("08567-040");

        // when
        Endereco e = g.fromJson(endereco, Endereco.class);

        // then
        assertThat(e.getCep()).isEqualTo("08567-040");
    }
}
