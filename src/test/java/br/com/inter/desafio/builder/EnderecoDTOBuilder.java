package br.com.inter.desafio.builder;

import br.com.inter.desafio.dto.EnderecoDTO;
import lombok.Builder;

@Builder
public class EnderecoDTOBuilder {

    @Builder.Default
    private Long id = 1L;

    @Builder.Default
    private String cep = "08567-040";

    @Builder.Default
    private String logradouro = "Rua Maria Sanches Gambogi";

    @Builder.Default
    private String complemento = "";

    @Builder.Default
    private String bairro = "Jardim Madre Ângela";

    @Builder.Default
    private String localidade = "Poá";

    @Builder.Default
    private String uf = "SP";

    public EnderecoDTO toEnderecoDTO() {

        return new EnderecoDTO(
                id,
                cep,
                logradouro,
                complemento,
                bairro,
                localidade,
                uf);
    }
}
