package br.com.inter.desafio.utils;

import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EnderecoUtils {

    private final String URL_VIACEP = "https://viacep.com.br/ws/";

    private JasonUtils strToJson;

    private RestTemplate restTemplate = new RestTemplate();

    public String getCep(String cep) throws FormatoDeCepErradoException, CepNaoEncontrado {

        validaCep(cep);

        String uri = URL_VIACEP + cep;
        String endereco = restTemplate.getForObject(uri +"/json/", String.class);

        return endereco;
    }

    private void validaCep(String cep) throws FormatoDeCepErradoException, CepNaoEncontrado {
        String mascaraCep = "\\d\\d\\d\\d\\d-\\d\\d\\d";
        String mascaraCepSemTraco = "\\d\\d\\d\\d\\d\\d\\d\\d";

        boolean validaSemTraco = cep.matches(mascaraCepSemTraco);
        boolean validaComTraco = cep.matches(mascaraCep);

        if(validaComTraco) {
            cep.replace("-","");
        } else if(!validaSemTraco) {
            throw new FormatoDeCepErradoException(cep);
        } else {
            throw new CepNaoEncontrado(cep);
        }
    }
}
