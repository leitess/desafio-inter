package br.com.inter.desafio.utils;

import br.com.inter.desafio.models.Desafio;
import br.com.inter.desafio.models.Endereco;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JasonUtils {

    @Autowired
    private Gson gson;

    public Endereco stringToJson(String str) {
        Endereco endereco = gson.fromJson(str, Endereco.class);

        return endereco;
    }

    public String DesafioToJson(Desafio desafio) {

        return gson.toJson(desafio);
    }
}
