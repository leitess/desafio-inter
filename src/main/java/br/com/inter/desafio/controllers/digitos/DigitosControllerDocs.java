package br.com.inter.desafio.controllers.digitos;

import br.com.inter.desafio.dto.DigitoDTO;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.models.Digito;
import io.swagger.annotations.*;

import java.util.List;

@Api("Digito Unico")
public interface DigitosControllerDocs {

    @ApiOperation(value = "Calcula o digito unico")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o digito unico e o numero entrado para ser feito calculo"),
            @ApiResponse(code = 400, message = "Numeros não está de acordo com o esperado"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    Digito postDigitoUnico(@ApiParam(name = "numero", type = "String", value = "Numero a calcular", example = "9875",required = true) String numero,
                              @ApiParam(name = "vezesConcatenada", type = "Integer", value = "Vezes que o numero Inserido será calculado", example = "4", required = true) Integer vezesConcatenada) throws ValorMaxException, FormatoDeCepErradoException, ValorMinException;

    @ApiOperation(value = "Lista todos os digitos.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de todos digitos unicos calculados"),
    })
    List<Digito> listAll();

    @ApiOperation(value = "Apaga todos os digitos unicos calculados")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Historico limpado com sucesso"),
            @ApiResponse(code = 500, message = "Deu algum erro!")
    })
    void deleteAll();
}
