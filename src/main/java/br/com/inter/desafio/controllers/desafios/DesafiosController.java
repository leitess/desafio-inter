package br.com.inter.desafio.controllers.desafios;

import br.com.inter.desafio.dto.DesafioDTO;
import br.com.inter.desafio.dto.EntidadeDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.models.Desafio;
import br.com.inter.desafio.models.Entidade;
import br.com.inter.desafio.services.DesafioService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DesafiosController implements DesafiosControllerDocs {

    private DesafioService desafioService;


    @Override
    @PostMapping(value = "/desafio", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public EntidadeDTO postDesafio(@RequestParam String numero,
                                   @RequestParam Integer vezesConcatenada,
                                   @RequestParam String cep) throws ValorMaxException, FormatoDeCepErradoException, ValorMinException, CepNaoEncontrado {

        return desafioService.postDesafio(numero, vezesConcatenada, cep);
    }

    @Override
    @GetMapping(value = "/desafios", produces = "application/json")
    public List<EntidadeDTO> listAll() {

        return desafioService.listAll();
    }

    @Override
    @DeleteMapping("/desafios/limpar")
    public void deleteAll() {

        desafioService.deleteAll();
    }
}
