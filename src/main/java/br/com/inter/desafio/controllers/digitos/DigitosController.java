package br.com.inter.desafio.controllers.digitos;

import br.com.inter.desafio.dto.DigitoDTO;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.services.DigitoService;

import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DigitosController implements DigitosControllerDocs {

    @Autowired
    private DigitoService digitoService;

    @Override
    @PostMapping(value = "/digito", produces = "application/json")
    public Digito postDigitoUnico(@RequestParam String numero,
                                     @RequestParam Integer vezesConcatenada) throws ValorMaxException, FormatoDeCepErradoException, ValorMinException {

        return digitoService.digitoUnico(numero, vezesConcatenada);
    }

    @Override
    @GetMapping(value = "/digitos", produces = "application/json")
    public List<Digito> listAll() {

        return digitoService.listAll();
    }

    @Override
    @DeleteMapping("/digitos/limpar")
    public void deleteAll() {

        digitoService.deleteAll();
    }
}
