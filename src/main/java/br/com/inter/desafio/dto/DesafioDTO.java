package br.com.inter.desafio.dto;

import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.models.Endereco;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DesafioDTO {

    @ApiModelProperty(value = "Data Transfer Object - Desafio: Id")
    private Long id;

    @ApiModelProperty(value = "Data Transfer Object - Desafio: digito unico")
    @NotNull
    private Digito digitoUnico;

    @ApiModelProperty(value = "Data Transfer Object - Desafio: Endereço")
    @NotNull
    private Endereco endereco;

    public DesafioDTO(Digito digitoUnico, Endereco endereco) {
        this.digitoUnico = digitoUnico;
        this.endereco = endereco;
    }
}
