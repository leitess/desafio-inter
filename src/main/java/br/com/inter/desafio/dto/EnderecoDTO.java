package br.com.inter.desafio.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO {

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Id")
    private Long id;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Cep usado para consulta")
    @NotNull
    private String cep;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Logradouro encontrado")
    @NotNull
    private String logradouro;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Complemento encontrado")
    @NotNull
    private String complemento;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Bairro encontrado")
    @NotNull
    private String bairro;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Municipio/Cidade encontrada")
    @NotNull
    private String localidade;

    @ApiModelProperty(value = "Data Transfer Object - Endereço: Estado encontrado")
    @NotNull
    private String uf;
}
