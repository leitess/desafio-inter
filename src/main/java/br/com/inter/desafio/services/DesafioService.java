package br.com.inter.desafio.services;

import br.com.inter.desafio.dto.DesafioDTO;
import br.com.inter.desafio.dto.EntidadeDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.mapper.DesafioMapper;
import br.com.inter.desafio.mapper.EntidadeMapper;
import br.com.inter.desafio.models.Desafio;
import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.models.Endereco;
import br.com.inter.desafio.models.Entidade;
import br.com.inter.desafio.repository.DesafioRepository;
import br.com.inter.desafio.repository.EntidadeRepository;
import br.com.inter.desafio.utils.EnderecoUtils;
import br.com.inter.desafio.utils.JasonUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DesafioService {

    private DigitoService digitoService;

    private EnderecoService enderecoService;

    private DesafioRepository desafioRepository;

    private EntidadeRepository entidadeRepository;

    private EnderecoUtils enderecoUtils;

    private JasonUtils strToJson;

    private final DesafioMapper desafioMapper = DesafioMapper.INSTANCE;

    private final EntidadeMapper entidadeMapper = EntidadeMapper.INSTANCE;

    public EntidadeDTO postDesafio(String numero, Integer vezesConcatenadas, String cep) throws ValorMaxException, ValorMinException, FormatoDeCepErradoException, CepNaoEncontrado {

        Digito digitoUnico = digitoService.digitoUnico(numero, vezesConcatenadas);

        String enderecoEncontrado = enderecoUtils.getCep(cep);
        Endereco endereco = strToJson.stringToJson(enderecoEncontrado);

        DesafioDTO desafioDTO = new DesafioDTO(digitoUnico, endereco);

        Desafio desafio = desafioMapper.toModel(desafioDTO);
        Entidade entidade = new Entidade(desafio);
        Entidade entidadeSalva = entidadeRepository.save(entidade);

        return entidadeMapper.toDTO(entidadeSalva);

    }

    public List<EntidadeDTO> listAll() {

        return entidadeRepository.findAll()
                .stream()
                .map(entidadeMapper::toDTO)
                .collect(Collectors.toList());
    }

    public void deleteAll()  {
        entidadeRepository.deleteAll();
    }
}
