package br.com.inter.desafio.repository;

import br.com.inter.desafio.models.Desafio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesafioRepository extends JpaRepository<Desafio, Long> {
}
