package br.com.inter.desafio.repository;

import br.com.inter.desafio.models.Digito;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DigitoRepository extends JpaRepository<Digito, Long> {
}
