package br.com.inter.desafio.repository;

import br.com.inter.desafio.models.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
}
