package br.com.inter.desafio.repository;

import br.com.inter.desafio.models.Entidade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntidadeRepository extends JpaRepository<Entidade, Long> {
}
