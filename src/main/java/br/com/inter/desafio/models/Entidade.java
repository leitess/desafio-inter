package br.com.inter.desafio.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Entidade {

    @ApiModelProperty(value = "Código do desafio resolvido")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Os desafios: digito unico e endereço")
    @ManyToOne(cascade = CascadeType.ALL)
    private Desafio desafio;

    public Entidade(Desafio desafio) {
        this.desafio = desafio;
    }
}
