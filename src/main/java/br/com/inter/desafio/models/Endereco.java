package br.com.inter.desafio.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Builder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@Entity
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Endereco {

    @ApiModelProperty(value = "Codigo do endereço encontrado")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Cep do endereço")
    @Column
    private String cep;

    @ApiModelProperty(value = "Logradouro do cep consultado")
    @Column
    private String logradouro;

    @ApiModelProperty(value = "Complemento do endereço")
    @Column
    private String complemento;

    @ApiModelProperty(value = "Bairro do cep consultado")
    @Column
    private String bairro;

    @ApiModelProperty(value = "Municipio/cidade do cep consultado")
    @Column
    private String localidade;

    @ApiModelProperty(value = "Estado do cep consultado")
    @Column
    private String uf;

    public Endereco(String cep, String logradouro, String complemento, String bairro, String localidade, String uf) {
        this.cep = cep;
        this.logradouro = logradouro;
        this.complemento = complemento;
        this.bairro = bairro;
        this.localidade = localidade;
        this.uf = uf;
    }
}
