package br.com.inter.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FormatoDeCepErradoException extends Exception {

    public FormatoDeCepErradoException(String cep) {
        super(String.format("Cep %s está digitado em um formato errado", cep));
    }
}
