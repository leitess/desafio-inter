package br.com.inter.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValorMaxException extends Exception {

    public ValorMaxException(Integer num) {
        super(String.format("Alto lá! Acima do permitido. Você inseriu %d!!", num));
    }
}
