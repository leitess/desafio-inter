package br.com.inter.desafio.mapper;

import br.com.inter.desafio.dto.DesafioDTO;
import br.com.inter.desafio.models.Desafio;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DesafioMapper {
    DesafioMapper INSTANCE = Mappers.getMapper(DesafioMapper.class);

    Desafio toModel(DesafioDTO desafioDTO);

    DesafioDTO toDTO(Desafio desafio);
}
