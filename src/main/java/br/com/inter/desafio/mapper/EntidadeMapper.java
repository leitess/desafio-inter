package br.com.inter.desafio.mapper;

import br.com.inter.desafio.dto.EntidadeDTO;
import br.com.inter.desafio.models.Entidade;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EntidadeMapper {

    EntidadeMapper INSTANCE = Mappers.getMapper(EntidadeMapper.class);

    Entidade toModel(EntidadeDTO entidadeDTO);

    EntidadeDTO toDTO(Entidade entidade);
}
