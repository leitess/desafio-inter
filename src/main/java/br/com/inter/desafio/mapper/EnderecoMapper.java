package br.com.inter.desafio.mapper;

import br.com.inter.desafio.dto.EnderecoDTO;
import br.com.inter.desafio.models.Endereco;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EnderecoMapper {

    EnderecoMapper INSTANCE = Mappers.getMapper(EnderecoMapper.class);

    Endereco toModel(EnderecoDTO enderecoDTO);

    EnderecoDTO toDTO(Endereco endereco);
}
