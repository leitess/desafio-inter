# Desafio Inter

**Objetivo:** Desafio que combina dois recursos: um  recurso descobre o digito unico de um determinado numero e o numero de vezes que esse numero será concatenado e outro recurso que encontra o endereço de um dado cep.

## Compilar e Executar
```
./mvnw spring-boot:run
```


## Testar
- Testa todas as classe de teste (e todos os testes dessas classes): `./mvnw test`

- Testa apenas uma classe:

```
./mvnw -Dtest=DigitoUnicoUtilsTests test

./mvnw -Dtest=EnderecoUtilsTests test

./mvnw -Dtest=EnderecosControllerTests test

./mvnw -Dtest=DigitosControllerTests test
```

- Testa apena um teste (dentro de uma classe de teste):
``` 
./mvnw -Dtest=DigitoUnicoUtilsTests#dadoUmNumero_QuandoAFuncaoDigitoUnicoForChamada_ResultarNoValorCorreto test

./mvnw -Dtest=DigitoUnicoUtilsTests#dadoUmNumeroEAsVezesQueSeraConcatenada_QuandoAFuncaoVezesConcatenadaForChamada_ResultarNaStringCorreta test

./mvnw -Dtest=DigitoUnicoUtilsTests#dadoUmNumeroEAsVezesQueSeraConcatenado_ChamarAFuncaoDeConcatenarECalcularODigitoUnico_RetornarODigitoUnico test

---------------------------------------------------------------------------------------------------------------------------------------------------------------

./mvnw -Dtest=EnderecoUtilsTests#quandoForDadoUmObjetoString_ChamarAFuncaoToJson_RetornUmObjetoJson test

---------------------------------------------------------------------------------------------------------------------------------------------------------------

./mvnw -Dtest=EnderecosControllerTests#quandoPOST_AFuncaoParaAnalizarOCepFoiChamada_EntaoRetornarOKStatusEOEndereco test

./mvnw -Dtest=EnderecosControllerTests#quandoGET_listaComEnderecoEChamada_EntaoRentornaOKStatus test

./mvnw -Dtest=EnderecosControllerTests#quandoGET_listaComEnderecoVaziaEChamada_EntaoRentornaOKStatus test

./mvnw -Dtest=EnderecosControllerTests#dadoUmCepInesperado_QuandoProcurarPeloEndereco_RetornarStatusBadRequestELancarAExcecao test

./mvnw -Dtest=EnderecosControllerTests#dadoUmCepIncorreto_QuandoProcurarPeloEndereco_RetornarStatusNotFoundELancarExcecao test

./mvnw -Dtest=EnderecosControllerTests#quandoOUsuarioQuiserLimparOHistoricoDeEnderecosEncontrados_RetornarOkStatus test
----------------------------------------------------------------------------------------------------------------------------------------------------------------

./mvnw -Dtest=DigitosControllerTests#dadoUmNumeroEAVezesParaConcatenar_chamarAFuncaoDigitoUnico_RetornarOkStatusEOCalculo test

./mvnw -Dtest=DigitosControllerTests#dadoUmNumeroMenorQueUm_QuandoCamadoADigitoUnico_RetornarStatusBadRequestELancarAExcecao test

./mvnw -Dtest=DigitosControllerTests#dadoUmNumeroMaiorQueOMaximoDoInteiro_QuandoCamadoADigitoUnico_RetornarStatusBadRequestELancarAExcecao test

./mvnw -Dtest=DigitosControllerTests#quandoOUsuarioQuiserLimparOHistoricoDeCalculos_RetornarOkStatus test
```
## Anotações
 
- Para consultar um endereço de um determinado cep e realizar o calculo do digito unico de um numero concatenado determinadas vezes: http://localhost:8080/api/desafio

- Para consultar todos os desafios realizados anteriormente: http://localhost:8080/api/desafios

- Para limpar todo o historico de desafios: http://localhost:8080/api/desafios/limpar

---

- Para realizar o calculo do digito unico de um numero concatenado determinadas vezes: http://localhost:8080/api/digito

- Para consultar o resultado dos calculos e o numero usado para calculo: http://localhost:8080/api/digitos

- Para limpar todo o historico de digitos calculados: http://localhost:8080/api/digitos/limpar

---

- Para consultar um endereço de um determinado cep: http://localhost:8080/api/endereco

- Para consultar todos os endereços procurado anteriormente: http://localhost:8080/api/enderecos

- Para limpar todo o historico de endereços procurados: http://localhost:8080/api/enderecos/limpar

---

- No endpoint para listar os desafios, somente irá aparecer os recursos colocados pela rota: http://localhost:8080/api/desafio. Pelo contrario, ao listar todos os digitos (http://localhost:8080/api/digitos) unicos calculados e todos os endereço procurados (http://localhost:8080/api/enderecos)
é possivel ver os que foram colocados pela rota: http://localhost:8080/api/desafio.

- Os comandos `./mvnw` podem ser substituido pelo comando `mvn`. 

- A url acessar o **H2 Console** é: http://localhost:8080/h2-console
  
  - A string de conexão do BD é: `jdbc:h2:mem:dbDesafio`
  - O usuario é: `sa`
  - Não tem senha (pode deixar o campo em branco).  